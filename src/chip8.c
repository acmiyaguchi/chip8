#include <stdlib.h>
#include <string.h>
#include "chip8.h"

// Create a new instance of the chip8 core
chip8_t* c8_create(void) {
    chip8_t* c8 = malloc(sizeof(chip8_t));
    return c8;
}

// Free the chip8 core
void c8_destroy(chip8_t* c8) {
    free(c8);
}

// Loads a rom into memory and pushes a new window out
void load_rom(chip8_t* c8, const char* filename) {
    memset(c8->display_buffer, 0, C8_DISP_BUFLEN);
}

// Draw the c8 display
void draw_display(chip8_t* c8) {
}

// Get the next opcode
uint8_t get_opcode(chip8_t* c8) {
    return c8->mem[c8->pc];
}

// Execute the next opcode
void execute_opcode(chip8_t* c8, uint8_t op) {
    //Switch based on the first nible
    switch((nib(op))) {
    case 0x0:
        if(op == 0x00e0) {
            // 00E0 - CLS: Clear screen
            break;
        }
        else if(op == 0x00ee) {
            // 00EE - RET: Return from a subroutine
            c8->pc = c8->stack[c8->sp--];
            break;
        }
        break;
    case 0x1:
        // 1nnn - JP addr: Jump to location nnn
        c8->pc = nnn(op);
        break;
    case 0x2:
        // 2nnn - CALL addr: Call subroutine at nnn
        c8->stack[++c8->sp] = c8->pc;
        c8->pc = nnn(op);
        break;
    case 0x3:
        // 3xkk - SE Vx, byte: Skip next instruction if Vx = kk
        if(c8->reg[xnib(op)] == kk(op))
            c8->pc += 2;
        break;
    case 0x4:
        // 4xkk - SNE Vx, byte: Skip next instruction if Vx != kk
        if(c8->reg[xnib(op)] != kk(op))
            c8->pc += 2;
        break;
    case 0x5:
        // 5xy0 - SE Vx, Vy: Skip if equal
        if((op & 0xf) == 0) {
            if(c8->reg[xnib(op)] == c8->reg[ynib(op)])
                c8->pc += 2;
        }
        break;
    case 0x6:
        // 6xkk - LD Vx, byte: Load
        c8->reg[xnib(op)] = kk(op);
        break;
    case 0x7:
        // 7xkk - ADD Vx, byte: Add to register
        c8->reg[xnib(op)] += kk(op);
        break;
    case 0x8:
        //Arithmetic functions
        switch(op & 0xf) {
        case 0x0:
            // 8xy0 - LD Vx, Vy: Load register to register
            c8->reg[xnib(op)] = c8->reg[ynib(op)];
            break;
        case 0x1:
            // 8xy1 - OR Vx, Vy: Bitwise or
            c8->reg[xnib(op)] |= c8->reg[ynib(op)];
            break;
        case 0x2:
            // 8xy2 - AND Vx, Vy: Bitwise and
            c8->reg[xnib(op)] &= c8->reg[ynib(op)];
            break;
        case 0x3:
            // 8xy3 - XOR Vx, Vy: Bitwise xor
            c8->reg[xnib(op)] ^= c8->reg[ynib(op)];
            break;
        case 0x4:
            // 8xy4 - ADD Vx, Vy: Addtion
        {
            int sum = c8->reg[xnib(op)] + c8->reg[ynib(op)];
            if(sum > 255)
                c8->reg[0xf] = 1;
            else
                c8->reg[0xf] = 0;
            c8->reg[xnib(op)] = sum & 0xffff;
        }
        break;
        case 0x5:
            // 8xy5 - SUB Vx, Vy: Subtraction
        {
            uint8_t x = c8->reg[xnib(op)];
            uint8_t y = c8->reg[ynib(op)];
            if(x > y)
                c8->reg[0xf] = 1;
            else
                c8->reg[0xf] = 0;
            c8->reg[xnib(op)] = x - y;
        }
        break;
        case 0x6:
            // 8xy6 - SHR Vx {, Vy}: Shift right
            if(((c8->reg[xnib(op)] >> 7 ) & 1) == 1)
                c8->reg[0xf] = 1;
            else
                c8->reg[0xf] = 0;
            c8->reg[xnib(op)] >>= 1;
            break;
        case 0x7:
            // 8xy7 - SUBN Vx, Vy: Subtract without borrowing
        {
            uint8_t x = c8->reg[xnib(op)];
            uint8_t y = c8->reg[ynib(op)];
            if(y > x)
                c8->reg[0xf] = 1;
            else
                c8->reg[0xf] = 0;
            c8->reg[xnib(op)] = y - x;
        }
        break;
        case 0xe:
            // 8xyE - SHL Vx {, Vy}: Shift left
            if(((c8->reg[xnib(op)]>>7) & 1) == 1)
                c8->reg[0xf] = 1;
            else
                c8->reg[0xf] = 0;
            c8->reg[xnib(op)] <<= 1;
            break;
        default:
            break;
        }
        break;
    case 0x9:
        // 9xy0 - SNE Vx, Vy: Skip if not equal
        if(op & 0xf)
            if(c8->reg[xnib(op)] != c8->reg[ynib(op)])
                c8->pc += 2;
        break;
    case 0xa:
        // Annn - LD I, addr: Set I to nnn
        c8->I = nnn(op);
        break;
    case 0xb:
        // Bnnn - JP V0, addr: Jump to nnn + V0
        c8->pc = nnn(op) + c8->reg[0];
        break;
    case 0xc:
        // Cxkk - RND Vx, byte: Random byte & kk
        c8->reg[xnib(op)] = (rand() % 256) & kk(op);
        break;
    case 0xd:
        // Dxyn - DRW Vx, Vy, nibble: Draw sprite
        //TODO
        break;
    case 0xe:
        // Ex9E - SKP Vx: Skip if keyboard is equal
        if(kk(op) == 0x9e) {
            //skip if equal
        }
        // ExA1 - SKNP Vx: Skip if keyboard value is not eqal
        else if(kk(op) == 0xa1) {
            //skip if not equal
        }
        break;
    case 0xf:
        switch(kk(op)) {
        case 0x7:
            // Fx07 - LD Vx, DT: Delay timer
            c8->reg[xnib(op)] = c8->delay_timer;
        case 0xa:
            // Fx0A - LD Vx, K: Blocking keypress
            break;
        case 0x15:
            // Fx15 - LD DT, Vx: Set the delay timer
            //TODO
            break;
        case 0x18:
            // Fx18 - LD ST, Vx: Set the sound timer
            c8->sound_timer = c8->reg[xnib(op)];
        case 0x1e:
            //Fx1E - ADD I, Vx: Add to the instruction counter
            c8->I += c8->reg[xnib(op)];
            break;
        case 0x29:
            // Fx29 - LD F, Vx: Load locations of sprites
            //TODO
            break;
        case 0x33:
            //  Fx33 - LD B, Vx: BCD representation
            break;
        case 0x55:
            // Fx55 - LD [I], Vx: Store all registers in memory
            break;
        case 0x65:
            // Fx65 - LD Vx, [I]: Read into registers from memory
            break;
        }
        break;
    default:
        break;
    }
}
