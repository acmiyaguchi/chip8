#ifndef CHIP8_H_GUARD
#define CHIP8_H_GUARD

#define C8_RAM_SIZE        4096
#define C8_ROM_OFFSET      0x200
#define C8_DISP_HEIGHT    32
#define C8_DISP_WIDTH    64
#define C8_DISP_BUFLEN    2048

#define nnn(OP)     ((OP) & 0xfff)
#define nib(OP)     (((OP) >> 12) & 0xff)
#define xnib(OP)    (((OP) >> 8) & 0xff)
#define ynib(OP)    (((OP) >> 4) & 0xff)
#define kk(OP)      ((OP) & 0xff)

#include <stdint.h>

//define struct for our class
struct chip8 {
    // CPU information
    uint8_t mem[C8_RAM_SIZE];
    uint8_t display_buffer[C8_DISP_BUFLEN]; //64*32 display
    uint8_t reg[0x10];
    uint16_t I;
    uint16_t stack[0x10];
    uint16_t pc;
    uint8_t sp;
    uint8_t delay_timer;
    uint8_t sound_timer;
};

typedef struct chip8 chip8_t;

chip8_t* c8_create(void);
void c8_destroy(chip8_t* c8);
void load_rom(chip8_t* c8, const char* filename);
void draw_display(chip8_t* c8);
uint8_t get_opcode(chip8_t* c8);
void execute_opcode(chip8_t* c8, uint8_t op);

#endif //CHIP8_H_GUARD
